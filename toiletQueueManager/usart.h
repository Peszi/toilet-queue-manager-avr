/*
 * usart.h
 *
 * Created: 04.04.2019 20:40:01
 *  Author: Peszi
 */ 


#ifndef USART_H_
#define USART_H_

#ifndef USART_BAUD
	#define USART_BAUD 19200
#endif

#define USART_BAUD_PRESCALE (((F_CPU / (USART_BAUD * 16UL))) - 1)

void USART_init(void);

void USART_flush(const char *buffer);

unsigned char USART_listen(const char *buffer);

#endif /* USART_H_ */