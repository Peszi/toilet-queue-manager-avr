/*
 * toiletQueueManager.c
 *
 * Created: 04.04.2019 20:08:20
 * Author : Peszi
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "usart.h"

#define DIODE_PIN 0

char buffer[32];

int main(void) {
	
	USART_init();
	
	DDRB |= (1 << DIODE_PIN);
	
	sprintf(buffer, "ATH0\r\n");
	
    while (1) {
		PORTB ^= (1 << DIODE_PIN);
		_delay_ms(500);
		
		USART_flush(buffer);
    }
}

