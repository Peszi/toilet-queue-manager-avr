/*
 * usart.c
 *
 * Created: 04.04.2019 20:39:44
 *  Author: Peszi
 */ 

#include <avr/io.h>
#include <string.h>
#include "usart.h"

void USART_init(void) {
	UBRR0H = (USART_BAUD_PRESCALE >> 8);
	UBRR0L = USART_BAUD_PRESCALE;
	
	UCSR0B |= (1 << RXEN0)|(1 << TXEN0);
	UCSR0C |= (1 << UCSZ00)|(1 << UCSZ01);//|(1 << UMSEL00);	// 8bit
}

void USART_flush(const char *buffer) {
	const unsigned char buffer_str_len = strlen(buffer);
	unsigned char buffer_idx = 0;
	while (buffer_idx < buffer_str_len) {
		while(!(UCSR0A & (1 << UDRE0)));
		UDR0 = buffer[buffer_idx++];
	}
	//memset(buffer, 0, buffer_str_len);
}

unsigned char USART_listen(const char *buffer) {
	const unsigned char buffer_size = sizeof(buffer);
	unsigned char buffer_idx = 0;
	unsigned long tick_count = 0;
	unsigned char data = 0;
	while (tick_count < F_CPU) {
		if (UCSR0A (1 << RXC0) {
			data = UDR0;
			if (data == '\n' || buffer_idx + 1 >= buffer_size) {
				break;
			}
			buffer[buffer_idx++] = data;
		}
		tick_count++;
	}
	return buffer_idx;
}